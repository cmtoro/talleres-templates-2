# Templates para los talleres

El presente repositorio alberga la estructura/plantillas para desarrollar cada taller del curso.

* [Taller 1 - Aplicaciones de consola](taller1/README.md)
* [Taller 2 - Genericidad y Enumeraciones](taller2/README.md)
* [Taller 3 - Listas y Pilas](taller3/README.md)
* [Taller 4 - Ordenamiento](taller4/README.md)
* [Taller 5 - Colas de Prioridad](taller5/README.md)
* [Taller 6 - Árboles](taller6/README.md)
* [Taller 7 - Tablas de Hash](taller7/README.md)
* [Taller 8 - Grafos no dirigidos](taller8/README.md)
* [Taller 9 - Grafos dirigidos](taller9/README.md)
* [Taller 10 - Grafos y Algoritmos](taller10/README.md)