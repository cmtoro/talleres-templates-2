package estructuras;

/**
 * Representa las funcionalidades basicas que debe implementar un grafo.
 * @author SamuelSalazar
 * @param <T> Objeto que se desea guardar como nodos.
 */
public interface Grafo<T extends Nodo>
{
	/**
	 * Devuelve el nodo cuyo id coincide con el dado por parametro.
	 * @param id El identificador unico del nodo que se desea buscar
	 * @return Nodo que coincide con el id  o null si no hay coincidencias
	 */
	public Nodo buscarNodo(int id);
	
	/**
	 * Retorna todos los Arcos salientes del nodo con id dado.  
	 * @param id Identificador unico del nodo
	 * @return Arreglo con los arcos con los nodos adyacentes al nodo con id dado.
	 */
	public Arco[] darArcosOrigen(int id);
	
	/**
	 * Retorna todos los Arcos entrantes del nodo con id dado.  
	 * @param id Identificador unico del nodo
	 * @return Arreglo con los arcos entrantes al nodo con id dado.
	 */
	public Arco[] darArcosDestino(int id);
	
	/**
	 * Agrega un nodo al grafo
	 * @param nodo Nodo que se quiere agregar
	 * @return True si el nodo fue agregado, falso de lo contrario
	 */
	public boolean agregarNodo(T nodo);
	
	/**
	 * Agrega un arco desde un nodo inicio a un nodo fin con un costo dado 
	 * y un objeto como informacion adicional asociada
	 * @param inicio Id del nodo inicio
	 * @param fin Id del nodo final
	 * @param costo Costo de ir del nodo inicio al nodo fin
	 * @param obj Informacion adicional asociada al arco (Debe ser un objeto Comparable)
	 * @return True si el arco fue agregado, false de lo contrario.
	 */
	public <E extends Comparable<E>> boolean agregarArco(int inicio, int fin, double costo, E obj);
	
	/**
	 * Agrega un arco desde un nodo inicio a un nodo fin con un costo dado.
	 * @param inicio Id del nodo inicio
	 * @param fin Id del nodo final
	 * @param costo Costo de ir del nodo inicio al nodo fin
	 * @return True si el arco fue agregado, false de lo contrario.
	 */
	public boolean agregarArco(int inicio, int fin, double costo);
	
	/**
	 * Elimina el nodo con el identificador dado y todos los arcos relacionados con el nodo
	 * @param id El identificador del nodo a eliminar
	 * @return True si el nodo fue eliminado, false de lo contrario
	 */
	public boolean eliminarNodo(int id);
	
	/**
	 * Elimina el arco que conecta el nodo inicio con el nodo fin
	 * @param inicio Id del nodo inicial
	 * @param fin Id del nodo final
	 * @return El arco que fue eliminado, null si no se elimino ninguno
	 */
	public Arco eliminarArco(int inicio, int fin);
	
	/**
	 * Devuelve un arreglo con todos los arcos del grafo
	 * @return Arreglo de arcos
	 */
	public Arco[] darArcos();
	
	/**
	 * Devuelve un arreglo con todos los nodos del grafo
	 * @return Arreglo de nodos
	 */
	public Nodo[] darNodos();
}
