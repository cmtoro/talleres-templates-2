package estructuras;

import java.util.HashMap;
import java.util.List;

/**
 * Clase que representa un grafo con peso no dirigido.
 * @author SamuelSalazar
 * @param <T> El tipo que se va a utilizar como nodos del grafo
 */
public class GrafoNoDirigido<T extends Nodo> implements Grafo<T> {

	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos

	/**
	 * Lista de adyacencia 
	 */
	private HashMap<Integer, List<Arco>> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
	// Es libre de implementarlo con la representacion de su agrado. 

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() {
		//TODO implementar
	}

	@Override
	public boolean agregarNodo(T nodo) {
		//TODO implementar
		
	}

	@Override
	public boolean eliminarNodo(int id) {
		//TODO implementar
	}

	@Override
	public Arco[] darArcos() {
		//TODO implementar
	}

	private <E extends Comparable<E>> Arco crearArco(  int inicio,  int fin,  double costo, E e )
	{
		return new Arco(buscarNodo(inicio), buscarNodo(fin), costo, e);
	}

	@Override
	public Nodo[] darNodos() {
		//TODO implementar
	}

	@Override
	public <E extends Comparable<E>> boolean agregarArco(int i, int f, double costo, E obj) {
		//TODO implementar
	}

	@Override
	public boolean agregarArco(int i, int f, double costo) {
		return agregarArco(i, f, costo, null);
	}

	@Override
	public Arco eliminarArco(int inicio, int fin) {
		//TODO implementar
	}

	@Override
	public Nodo buscarNodo(int id) {
		//TODO implementar
	}

	@Override
	public Arco[] darArcosOrigen(int id) {
		//TODO implementar
	}

	@Override
	public Arco[] darArcosDestino(int id) {
		//TODO implementar
	}

}
