package interfaz;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import modelos.Paradero;
import mundo.AdministradorSITP;


public class Main 
{	
	/**
	 * Administrador del SITP
	 */
	private AdministradorSITP admin;

	/**
	 * Lector de la consola
	 */
	private BufferedReader in;

	/**
	 * Inicializa la clase principal de la aplicacion
	 */
	public Main() throws Exception {
		admin = new AdministradorSITP();
		in = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("<< Bienvenido al sistema integrado de transporte de Bogota >>");
	}

	/**
	 * Menu principal de la aplicación
	 * @throws Exception
	 */
	public void menuInicial() throws Exception
	{
		String mensaje = "Por favor seleccione  "
				+ "una opcion: \n - - - - - - - - - - - - - - - - - - - - -\n"
				+ "[1] Cargar informacion sistema \n"
				+ "[2] Buscar paradero por nombre \n"
				+ "[3] Buscar paradero por id \n"
				+ "[4] Rutas que pasan por un paradero \n"
				+ "[5] Distancia entre paraderos más cercanos \n"
				+ "[6] Distancia entre paraderos más lejanos \n"
				+ "[7] Salir \n";
		System.out.print(mensaje);
		String op1 = in.readLine();
		
		System.out.println(">> "+ op1);
		switch(op1)
		{
		case "1":
			try
			{
				admin.cargarInformacion();
			}
			catch(Exception e)
			{
				System.out.println(e instanceof IOException ? 
						"Ocurrio un error al leer los archivos. por favor intenta de nuevo"
						:"No fue posible extraer la informacion. Verifica el formato de los archivos");
			}
			break;
		case "2":
			System.out.println(">Ingrese el nombre del paradero");
			Paradero p = admin.buscarParadero(in.readLine().trim()); 
			System.out.println(p!=null ? p: "Paradero no encontrado");
			break;
		case "3": 
			System.out.println(">Ingrese el identificador del paradero");
			try{
				Paradero p1 = admin.darParadero(Integer.parseInt(in.readLine()));
				System.out.println(p1!=null ? p1: "Paradero no encontrado");
			}
			catch(Exception e)
			{
				System.out.println("El identificador ingresado no es valido.");
			}
			break;
		case "4": 
			System.out.println(">Ingrese el nombre del paradero");
			String paradero = in.readLine();
			String[] rutas =  admin.darRutasParadero(paradero);
			System.out.println("Por el paradero "+ paradero+" pasan "+ rutas.length+" ruta(s): ");
			for (int i = 0; i < rutas.length; i++) {
				System.out.println((i+1)+". "+rutas[i]);

			}
			break;
		case "5": 
			System.out.println("Los paraderos más cercanos están a "+admin.distanciaMinimaParaderos()+" metros de distancia");
			break;
		case "6": 
			System.out.println("Los paraderos más lejanos están a "+admin.distanciaMaximaParaderos()+" metros de distancia");
			break;
		case "7": System.exit(1);
		}
		System.out.println("<<");
	}
	
	public static void main(String[] args) throws Exception {
		Main main = new Main();
		for(;;)
		{
			main.menuInicial();
			System.out.println("Intro para volver al menú inicial");
			main.in.readLine();
		}
	}
}
