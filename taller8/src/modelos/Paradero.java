package modelos;

import estructuras.Nodo;

/**
 * Clase que representa un paradero del sistema integrado de transporte SITP
 * @author SamuelSalazar
 */
//TODO La clase debe implementar la interface Nodo
public class Paradero {

	/**
	 * Latitud del paradero
	 */
	//TODO declare el atributo latitud
	
	/**
	 * Longitud del paradero
	 */
	//TODO Declare el atributo longitud
	
	/**
	 * Nombre del paradero
	 */
	//TODO declare el nombre del paradero
	
	/**
	 * Identificador unico del paradero
	 */
	//TODO Declare el identificador del paradero
	
	
	/**
	 * Construye un nuevo paradero con un nombre e id dado en una ubicación
	 * representada por la latitud y la longitud.
	 * @param id Indentificador unico del paradero
	 * @param latitud 
	 * @param longitud 
	 * @param nombre
	 */
	public Paradero(int id, double latitud, double longitud, String nombre) {
		//TODO implementar
	}
	
	public int darId(){
		//TODO implementar
	}
	
	/**
	 * Devuelve la latitud del paradero
	 * @return latitud
	 */
	public double darLatitud() {
		//TODO implementar
	}
	
	/**
	 * Devuelve la longitud del paradero
	 * @return longitud
	 */
	public double darLongitud() {
		//TODO implementar
	}
	
	/**
	 * Devuelve el nombre del paradero
	 * @return nombre
	 */
	public String darNombre() {
		//TODO implementar
	}
	
	/**
	 * nombre (latitud, longitud)
	 */
	@Override
	public String toString() {
		return nombre+" ("+latitud+" , "+longitud+")";
	}

	@Override
	public int compareTo(Nodo o) 
	{
		return o.darId() - id;
	}
	
	
}
