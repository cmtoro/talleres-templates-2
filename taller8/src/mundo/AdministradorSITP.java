package mundo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Set;
import java.util.TreeSet;

import modelos.Paradero;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.Nodo;

/**
 * Clase que representa el administrador del sistema integrado de transporte
 * @author SamuelSalazar
 *
 */
public class AdministradorSITP {

	/**
	 * Ruta del archivo de paraderos
	 */
	public static final String RUTA_PARADEROS ="./data/paraderos.txt";
	
	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/rutas.txt";

	
	/**
	 * Grafo que modela el sistema integrado de transporte
	 */
	//TODO Declare el grafo que va a modela el sistema

	
	/**
	 * Construye un nuevo administrador del SITP
	 */
	public AdministradorSITP() 
	{
		//TODO inicialice el grafo como un GrafoNoDirigido
	}

	/**
	 * Devuelve todas las rutas que pasan por un paradero con nombre dado
	 * @param nombre 
	 * @return Arreglo con los nombres de las rutas que pasan por el paradero requerido
	 */
	public String[] darRutasParadero(String nombre)
	{
		//TODO Implementar
	}

	/**
	 * Devuelve la distancia que hay entre los paraderos más cercanos del sistema.
	 * @return distancia mínima entre 2 paraderos
	 */
	public double distanciaMinimaParaderos()
	{
		//TODO implementar
	}
	
	
	public Paradero darParadero(int id)
	{
		//TODO implementar
	}
	/**
	 * Devuelve la distancia que hay entre los paraderos más lejanos del sistema.
	 * @return distancia máxima entre 2 paraderos
	 */
	public double distanciaMaximaParaderos()
	{
		//TODO implementar
	}


	/**
	 * Metodo encargado de extraer la información de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		//TODO implementar
		
		System.out.println("Se han cargado correctamente "+grafo.darNodos().length+" paraderos");
		System.out.println("Se han cargado correctamente "+ grafo.darArcos().length+" arcos");
	}

	/**
	 * Busca el paradero con nombre dado<br>
	 * @param nombre el nombre del paradero buscado
	 * @return Paradero cuyo nombre coincide con el parametro, null de lo contrario
	 */
	public Paradero buscarParadero(String nombre)
	{
		//TODO implementar
	}

}
